/**
 * @Class:      JasperReportProperties
 * @Created:    11-06-2019
 * @Updated:    21-07-2019
 * @CopyRight:	VON Development Studio 2017
 */
package ec.com.von.common.jasperreport

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class JasperReportProperties(
  var medicalCenterName: String? = null,
  var medicalCenterContact: String? = null
)
