/**
 * @Class:      JasperReportType
 * @Created:    12-06-2019
 * @Updated:    21-07-2019
 * @CopyRight:  VON Development Studio 2017
 */
package ec.com.von.common.jasperreport

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
enum class JasperReportType {

  PDF;

  fun equals(type: String): Boolean {
    return type.toUpperCase() == PDF.name.toUpperCase()
  }
}
