/**
 * @Class:      JasperReportException
 * @Created:    12-06-2019
 * @Updated:    21-07-2019
 * @CopyRight:  VON Development Studio 2017
 */
package ec.com.von.common.jasperreport

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
data class JasperReportException(
  override val cause: Throwable? = null,
  override val message: String? = null,
  var reportName: String? = null
) : Exception()
