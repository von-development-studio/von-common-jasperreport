/**
 * @Class:      JasperReportPdfExporter
 * @Created:    12-06-2019
 * @Updated:    21-07-2019
 * @CopyRight:  VON Development Studio 2017
 */
package ec.com.von.common.jasperreport

import net.sf.jasperreports.engine.JREmptyDataSource
import net.sf.jasperreports.engine.JRException
import net.sf.jasperreports.engine.JasperRunManager
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
open class JasperReportPdfExporter<T>(
  homePath: String,
  reportsPath: String,
  clazz: Class<T>
) : JasperReportExporter<T>(homePath, reportsPath, clazz) {

  @Throws(JasperReportException::class)
  override fun export(information: List<Any>) {
    try {
      val finalDS = if (information.isEmpty()) {
        JREmptyDataSource()
      } else {
        JRBeanCollectionDataSource(information)
      }
      JasperRunManager.runReportToPdfStream(
        reportStream,
        outputStream,
        parameters,
        finalDS)
    } catch (ex: JRException) {
      throw JasperReportException(ex, ex.message!!, reportName!!)
    }
  }

}
