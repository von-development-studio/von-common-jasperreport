/**
 * @Class:      JasperReportExporter
 * @Created:    10-06-2019
 * @Updated:    21-07-2019
 * @CopyRight:  VON Development Studio 2017
 */
package ec.com.von.common.jasperreport

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.io.ClassPathResource
import org.yaml.snakeyaml.Yaml
import java.io.*
import java.lang.StringBuilder
import javax.servlet.http.HttpServletResponse

/**
 * @author *Luis García Castro **(luis.garcia@von-ds.com)***
 */
abstract class JasperReportExporter<T>(
  private val homePath: String,
  private val reportsPath: String,
  private val clazz: Class<T>,
  private val log: Logger = LoggerFactory.getLogger(JasperReportExporter::class.java),
  protected val parameters: MutableMap<String, Any> = HashMap(),
  var reportName: String? = null,
  var reportStream: InputStream? = null,
  var outputStream: OutputStream? = null
) {
  init {
    val fontConfigPath = StringBuilder()
    fontConfigPath.append(System.getProperty("java.home"))
    fontConfigPath.append(File.separator)
    fontConfigPath.append("lib")
    fontConfigPath.append(File.separator)
    fontConfigPath.append("fontconfig.Prodimage.properties")
    val fontConfigFile = File(fontConfigPath.toString())
    if (fontConfigFile.exists()) {
      System.setProperty("sun.awt.fontconfig", fontConfigPath.toString())
    }

    try {
      val configurationInputStream = clazz.classLoader
        .getResourceAsStream("$homePath/configuration.yml")
      val jasperDefaults = Yaml().loadAs(configurationInputStream, JasperReportProperties::class.java)
      val homePathURI = clazz.classLoader.getResource("$homePath/")
      parameters["medicalCenterName"] = jasperDefaults.medicalCenterName!!
      parameters["medicalCenterContact"] = jasperDefaults.medicalCenterContact!!
      parameters["watermarkPath"] = "$homePathURI"
    } catch (ex: Exception) {
      throw JasperReportException(ex, "Jasper configuration.yml is not well defined")
    }
  }

  @Throws(JasperReportException::class)
  open fun setJasperPrint(reportName: String): JasperReportExporter<T> {
    try {
      this.reportName = reportName
      reportStream = clazz.classLoader.getResourceAsStream("${reportsPath}/${reportName}.jasper")
      return this
    } catch (ex: IOException) {
      throw JasperReportException(ex, "Jasper report does not exist", reportName)
    } catch (ex: NullPointerException) {
      throw JasperReportException(ex, "Jasper report does not exist", reportName)
    }

  }

  @Throws(JasperReportException::class)
  open fun setHttpResponse(res: HttpServletResponse): JasperReportExporter<T> {
    try {
      this.outputStream = res.outputStream
      return this
    } catch (ex: IOException) {
      throw JasperReportException(ex, "HttpResponse OutputStream error")
    }

  }

  open fun setParameters(parameters: Map<String, Any>): JasperReportExporter<T> {
    this.parameters.putAll(parameters)
    return this
  }

  @Throws(JasperReportException::class)
  abstract fun export(information: List<Any>)

}
